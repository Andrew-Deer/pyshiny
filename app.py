from shiny import App, render, ui
from nltk.book import *

app_ui = ui.page_fluid(
    ui.h1("Lexical ", ui.em("dispersion"), " plot"),
    ui.layout_sidebar(
        ui.panel_sidebar(
            ui.input_slider("n", "N", 0, 100, 20),
        ),
        ui.panel_main(
            ui.output_plot("dispersion_plot"),
        ),
    ),
)


def server(input, output, session):
    @output
    @render.plot(alt="A dispersion plot")
    def dispersion_plot():
        text2.dispersion_plot(["Elinor", "Marianne"])


app = App(app_ui, server)
